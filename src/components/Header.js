import React from 'react';
import {Link} from 'react-router-dom';

/**
 * @class Header
 */
class Header extends React.Component {
	/**
	 * @function render
	 * @return {JSX} JSX del Header
	 */
	render() {
		return (
			<nav className="navbar">
				<Link to="/" className="navbar-brand d-flex flex-row">
					<label>{this.props.appName} </label>
				</Link>
			</nav>
		);
	}
}

export default Header;
