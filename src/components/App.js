import React from 'react';
import {connect} from 'react-redux';
import Header from './Header';
import './App.css';

import {
	APP_LOAD,
} from '../constants/actionTypes';

const mapStateToProps = (state) => {
	return {
		appLoaded: state.common.appLoaded,
		appName: state.common.appName,
	};
};

const mapDispatchToProps = (dispatch) => ({
	onLoad: () =>
		dispatch({type: APP_LOAD}),
});

/**
 * @class App
 */
class App extends React.Component {
	/**
	 * @function componentDidMount
	 */
	componentDidMount() {
		this.props.onLoad();
	}

	/**
   * @function render
   * @return {JSX}
   */
	render() {
		if (this.props.appLoaded) {
			return (
				<div>
					<Header
						appName={this.props.appName} />
				</div>
			);
		}
		return (
			<div className="App">
			</div>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

